#!/usr/bin/env python3

import logging
import os.path
from datetime import datetime, timedelta

import caldav
import icalendar
import keyring
import yaml
from caldav.objects import dav

import filters


class Configuration:
    """Regroup user DavCal configuration"""

    def __init__(self, yaml_file):
        """Fetch DavCal from YAML configuration and keyring"""
        with open(yaml_file, 'r') as f:
            c = yaml.load(f, Loader=yaml.FullLoader)
            logging.info("Fetching {}".format(c['url']))
            password = keyring.get_password('sogo', c['username'])
            client = caldav.DAVClient(c['url'], username=c['username'],
                                      password=password)
            self.calendars = client.principal().calendars()
            self.selected_groups = c['selected_groups']
            if 'output_directory' in c.keys():
                self.output_directory = c['output_directory']
            else:
                self.output_directory = '.'

    def get_all_dav_components(self):
        """Return all components from the given calendars"""
        components = []
        for cal in self.calendars:
            n = cal.get_properties([dav.DisplayName(), ])['{DAV:}displayname']
            logging.info("Loading '\033[1;1m{}\033[1;0m'".format(n))
            from_date = datetime.today() - timedelta(weeks=1)  # Future events
            components += cal.date_search(from_date)
            components += cal.todos() + cal.journals()
        return components


def write_calendar_from_dav(f, dav_components, selected_groups):
    """Write a master calendar in file f using components from CalDav"""
    f.write(b'BEGIN:VCALENDAR\r\nPRODID:-//SaphSync//EN\r\nVERSION:2.0\r\n')
    for i, dav_component in enumerate(dav_components):
        print('Traitement de {}/{}\r'.format(i, len(dav_components)), end='')
        dav_component.load()  # Download CalDav component
        ical = icalendar.Event.from_ical(dav_component.data)  # Parse ical
        blacklist = ['VCALENDAR', 'DAYLIGHT', 'STANDARD']
        ical_components = [c for c in ical.walk() if c.name not in blacklist]
        for ical_component in ical_components:
            if ical_component.name == "VEVENT":
                # If it is an event, then check it is in the correct group
                if filters.group(ical_component, selected_groups):
                    f.write(ical_component.to_ical())
            else:
                f.write(ical_component.to_ical())
    f.write(b'END:VCALENDAR')


if __name__ == '__main__':
    # Configure logging system
    logging.addLevelName(logging.INFO, "\033[1;36mINFO\033[1;0m")
    logging.addLevelName(logging.WARNING, "\033[1;103mWARNING\033[1;0m")
    logging.addLevelName(logging.ERROR, "\033[1;41mERROR\033[1;0m")
    logging.basicConfig(
        level=logging.INFO,
        format='\033[1;90m%(asctime)s\033[1;0m %(levelname)s %(message)s'
    )

    conf = Configuration('config.yml')  # Load user configuration and calendars
    dav_components = conf.get_all_dav_components()  # Load components
    with open(os.path.join(conf.output_directory, 'calendar.ics'), 'wb') as f:
        write_calendar_from_dav(f, dav_components, conf.selected_groups)
