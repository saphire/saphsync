# SaphSync

SaphSync permet de synchroniser son agenda SAPHIRE en :

  * Enlevant les TP, TD et BE qui ne nous concerne pas ;
  * Enlevant les évènements passés ;
  * En fussionnant tout en un unique calendrier au format ICS.

Il a été testé avec :

  * Mozilla Thunderbird ;
  * Gnome Calendar (Gnome Desktop) ;
  * Orage (XFCE Desktop) ;
  * Google Calendar (web and Android) ;
  * ICSx5 (Android).

## Mise en place

### Installation des dépendances

SaphSync requiert quelques dépendances dont :

  * Python 3 ;
  * `keyring` pour gérer les trousseaux de clés ;
  * `yaml` pour lire la configuration YAML ;
  * `caldav` pour accèder au serveur caldav de l'ENS ;
  * `icalendar` pour écrire facilement un fichier iCalendar.

Il est nécessaire d'utiliser pour le moment `caldav==0.5.0` car la dernière version est cassée.

### Configuration

Il faut créer `config.yml` à partir de `config.yml.example`.
Dans ce fichier on défini le nom d'utilisateur ENS, ainsi que nos groupes de TD, TP ou BE.
Si on ne souhaite pas filtrer un cours, alors il faut supprimer ou commenter la ligne.

Ensuite il faut donner son mot de passe ENS dans son trousseau de clé système.
Pour cela, on peut le faire en Python de cette manière :

```Python
import keyring
keyring.get_password('sogo', 'identifiant')
```
Puis suivez les instructions pour renseigner votre mot de passe.

### Lancement

Placez vous dans le dossier où vous avez cloné le code et lancer le script :

```bash
./saphsync.py
```

Un fichier `calendar.ics` va être écrit.
C'est ce fichier que vous voulez ensuite utiliser et intégrer dans vos différentes applications.

## Exemples d'intégration

### Intégration à Gnome Calendar

Il peut être sympathique d'avoir ses évènements directement dans ses notifications de bureau Gnome.
Pour cela ouvrez l'application *Gnome Calendar* et dans `Calendar Settings` ajouter un calendrier
à partir d'un fichier local.

À chaque fois que SaphSync sera exécuté, le calendrier Gnome sera automatiquement mis à jour.

### Intégration à Google Calendar

Pour mettre son calendrier dans Google Calendar il faut qu'il soit publique sur internet.

On va utiliser le serveur Zamok du Cr@ns.

Copier le fichier `icalendar.ics` vers votre page publique Zamok :

```bash
scp calendar.ics zamok.crans.org:~/www/
```

Votre calendrier est maintenant accessible à <https://perso.crans.org/pseudo/calendar.ics>.

Ouvrez maintenant les paramètres de Google Calendar Web
puis aller dans [Ajouter un calendrier à partir d'une URL](https://calendar.google.com/calendar/r/settings/addbyurl).

Voilà :)

### Intégration à Orage

Orage est une application de calendrier très légère du projet XFCE.
Il est possible de directement lui fournir le fichier ICS en entrée et
ensuite il suffira de cliquer sur le calendrier dans la zone des notifications
pour voir ses évènements.
