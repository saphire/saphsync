import logging
import re

logger = logging.getLogger('Filters')


def group(event, selected_groups) -> bool:
    """Filter group according to the group selected"""
    summary = event.get('summary')
    date = event.get('dtstart').dt.strftime("%Y-%m-%d")
    summary_search = re.search('([0-9]{3}) - ([A-Za-z ]*) \(grpe : ([^\)]*)\)',
                               summary, re.IGNORECASE)

    if summary_search:
        ue, groups = 'ue_' + summary_search.group(1), summary_search.group(
            3).split(' - ')
        if ue in selected_groups:
            # UE is configured
            for group in groups:
                if group in selected_groups[ue]:
                    # Group was selected
                    return True

            # Group is not the one selected
            logger.info(f"Remove '\033[1;1m{summary}\033[1;0m' (on \033[1;1m{date}\033[1;0m) because it had not been chosen")
            return False

    # If there is not a group specified do not filter
    return True
